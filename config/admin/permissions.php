<?php
return[
	'permission' => [
		'create' 	=> 'admin.permissions.create',
		'edit' 		=> 'admin.permissions.edit',
		'destroy' 	=> 'admin.permissions.delete',
		'trash' 	=> 'admin.permissions.trash',
		'undo' 		=> 'admin.permissions.undo',
		'list' 		=> 'admin.permissions.list',
		'audit'		=> 'admin.permissions.audit'
	],
	'role' => [
		'create' 	=> 'admin.roles.create',
		'edit' 		=> 'admin.roles.edit',
		'destroy' 	=> 'admin.roles.delete',
		'trash' 	=> 'admin.roles.trash',
		'undo' 		=> 'admin.roles.undo',
		'list' 		=> 'admin.roles.list',
		'audit'		=> 'admin.roles.audit',
		'show'		=> 'admin.roles.show',
	],
	'user' => [
		'create' 	=> 'admin.users.create',
		'edit' 		=> 'admin.users.edit',
		'destroy' 	=> 'admin.users.delete',
		'trash' 	=> 'admin.users.trash',
		'undo' 		=> 'admin.users.undo',
		'list' 		=> 'admin.users.list',
		'audit'		=> 'admin.users.audit',
		'show'		=> 'admin.users.show',
		'reset'		=> 'admin.users.reset',
	],
	'menu' => [
		'create' 	=> 'admin.menus.create',
		'edit' 		=> 'admin.menus.edit',
		'destroy' 	=> 'admin.menus.delete',
		'list' 		=> 'admin.menus.list',
	],

	'category' => [
		'create' 	=> 'admin.categories.create',
		'edit' 		=> 'admin.categories.edit',
		'destroy' 	=> 'admin.categories.delete',
		'list' 		=> 'admin.categories.list',
	],
	'tag' => [
		'create' 	=> 'admin.tags.create',
		'edit' 		=> 'admin.tags.edit',
		'destroy' 	=> 'admin.tags.delete',
		'list' 		=> 'admin.tags.list',
	],
	'article' => [
		'create' 	=> 'admin.articles.create',
		'edit' 		=> 'admin.articles.edit',
		'destroy' 	=> 'admin.articles.delete',
		'trash' 	=> 'admin.articles.trash',
		'undo' 		=> 'admin.articles.undo',
		'list' 		=> 'admin.articles.list',
		'audit'		=> 'admin.articles.audit',
		'show'		=> 'admin.articles.show',
	],
];